package io.github.clementi;

import java.util.Objects;

public class TermReference {

  private final DocumentId id;
  private final int position;

  public TermReference(DocumentId id, int position) {
    this.id = id;
    this.position = position;
  }

  public DocumentId getId() {
    return id;
  }

  public int getPosition() {
    return position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TermReference that = (TermReference) o;
    return position == that.position && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, position);
  }

  @Override
  public String toString() {
    return "TermReference{" + "id=" + id + ", position=" + position + '}';
  }
}
