package io.github.clementi;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class DocumentIndexer implements Indexer {

  @Override
  public Set<TermReference> index(Document document, Term term) {
    return getPositions(document, term).stream()
        .map(position -> new TermReference(document.getId(), position))
        .collect(Collectors.toUnmodifiableSet());
  }

  private Set<Integer> getPositions(Document document, Term term) {
    int termLength = term.getValue().length();
    int bodyLength = document.getBody().length();

    var indices = new HashSet<Integer>();

    for (int i = 0; i < bodyLength - termLength + 1; i++) {
      if (document.getBody().startsWith(term.getValue(), i)) {
        indices.add(i);
      }
    }

    return indices.stream().map(this::toPosition).collect(Collectors.toSet());
  }

  private int toPosition(int i) {
    return i + 1;
  }
}
