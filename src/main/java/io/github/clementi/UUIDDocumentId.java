package io.github.clementi;

import java.util.UUID;

public class UUIDDocumentId implements DocumentId {

  private final UUID value = UUID.randomUUID();

  private UUIDDocumentId() {}

  public static DocumentId create() {
    return new UUIDDocumentId();
  }

  @Override
  public String value() {
    return this.value.toString();
  }

  @Override
  public String toString() {
    return "UUIDDocumentId{" + "value=" + value + '}';
  }
}
