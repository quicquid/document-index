package io.github.clementi;

public interface DocumentId {

  String value();
}
