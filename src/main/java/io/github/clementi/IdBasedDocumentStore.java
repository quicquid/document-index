package io.github.clementi;

import java.util.HashMap;
import java.util.Map;

public class IdBasedDocumentStore implements DocumentStore {

  private final Map<DocumentId, Document> documents;

  public IdBasedDocumentStore() {
    this.documents = new HashMap<>();
  }

  @Override
  public void add(Document document) {
    this.documents.put(document.getId(), document);
  }

  @Override
  public Document getById(DocumentId id) {
    return this.documents.get(id);
  }

  @Override
  public int size() {
    return this.documents.size();
  }

  @Override
  public Iterable<Document> getAll() {
    return this.documents.values();
  }

}
