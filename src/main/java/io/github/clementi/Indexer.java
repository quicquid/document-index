package io.github.clementi;

import java.util.Set;

public interface Indexer {

  Set<TermReference> index(Document document, Term term);

}
