package io.github.clementi;

public interface Document {

  DocumentId getId();

  String getTitle();

  String getBody();
}
