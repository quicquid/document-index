package io.github.clementi;

import java.io.*;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Collectors;

public class FileDocument implements Document {

  private final String title;
  private final String body;

  private final DocumentId id;

  public FileDocument(DocumentId id, String title, Path path) throws IOException {
    this.id = id;
    this.title = title;

    try (var input = new FileInputStream(path.toFile())) {
      try (var reader = new BufferedReader(new InputStreamReader(input))) {
        body = reader.lines().collect(Collectors.joining());
      }
    }
  }

  @Override
  public DocumentId getId() {
    return this.id;
  }

  @Override
  public String getTitle() {
    return this.title;
  }

  @Override
  public String getBody() {
    return this.body;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FileDocument that = (FileDocument) o;
    return Objects.equals(title, that.title) && Objects.equals(body, that.body);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, body);
  }

  @Override
  public String toString() {
    return "FileDocument{" + "title='" + title + '\'' + ", id=" + id + '}';
  }
}
