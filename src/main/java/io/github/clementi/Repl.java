package io.github.clementi;

import static java.lang.System.out;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

public class Repl {

  public static void run(Index index, DocumentStore documents) throws IOException {
    try (var reader = new BufferedReader(new InputStreamReader(System.in))) {
      out.println("Welcome to the document index.");
      out.println("Enter a search term at the prompt.");

      printPrompt(documents.size());

      String line;
      while ((line = reader.readLine()) != null) {
        if (line.trim().isEmpty()) {
          printPrompt(documents.size());
          continue;
        }

        if (isCommand(line)) {
          handleCommand(line, index, documents);
          printPrompt(documents.size());
          continue;
        }

        var term = new Term(line);

        Map<DocumentId, Set<Integer>> results = index.search(term);

        if (results.isEmpty()) {
          out.println("No results");
        } else {
          out.printf("%d result(s):%n", results.size());
          results.forEach((documentId, positions) -> {
            var document = documents.getById(documentId);
            out.printf("%s, '%s', at %s%n", document.getId(), document.getTitle(), positions);
          });
        }

        printPrompt(documents.size());
      }
    }
  }

  private static boolean isCommand(String line) {
    return line.charAt(0) == ':';
  }

  private static void handleCommand(String command, Index index, DocumentStore documents) {
    if (command.equals(":terms")) {
      index.getTerms().forEach(out::println);
    } else if (command.equals(":refs")) {
      index.getDocumentReferences().forEach(out::println);
    } else if (command.equals(":docs")) {
      documents.getAll().forEach(out::println);
    } else if (command.equals(":help")) {
      out.println("Available commands are :terms, :refs, :docs, and :help");
    } else {
      out.printf("Command '%s' unknown%n", command);
    }
  }

  private static void printPrompt(int documentCount) {
    out.printf("index (%d)> ", documentCount);
  }
}
