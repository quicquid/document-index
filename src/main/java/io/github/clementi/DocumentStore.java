package io.github.clementi;

public interface DocumentStore {

  void add(Document document);

  Document getById(DocumentId id);

  int size();

  Iterable<Document> getAll();

}
