package io.github.clementi;

import java.io.IOException;
import java.nio.file.Path;

public class Main {

  public static void main(String[] args) throws IOException {
    Index index = new DocumentIndex(new DocumentIndexer());

    DocumentStore documents = new IdBasedDocumentStore();

    System.out.println("Adding documents...");

    Document doi = new FileDocument(UUIDDocumentId.create(), "Declaration of Independence",
        Path.of("./doi.txt"));
    documents.add(doi);
    index.add(doi);

    Document smile = new StringDocument(UUIDDocumentId.create(), "Candid Camera",
        "Smile! You're on candid camera on TV!");
    documents.add(smile);
    index.add(smile);

    Document preamble =
        new FileDocument(UUIDDocumentId.create(), "Preamble", Path.of("./preamble.txt"));
    documents.add(preamble);
    index.add(preamble);

    Document curious = new FileDocument(UUIDDocumentId.create(),
        "The curious incident of the dog in the night-time", Path.of("./curious.txt"));
    documents.add(curious);
    index.add(curious);

    Document articles =
        new FileDocument(UUIDDocumentId.create(), "Articles of Faith", Path.of("./articles.txt"));
    documents.add(articles);
    index.add(articles);

    Document needs =
        new StringDocument(UUIDDocumentId.create(), "Star Trek II: The Needs of the Many",
            "The needs of the many outweigh the needs of the few. Or the one.");
    documents.add(needs);
    index.add(needs);

    System.out.printf("%d documents added.%n", documents.size());

    Repl.run(index, documents);
  }
}
