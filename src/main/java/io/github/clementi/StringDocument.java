package io.github.clementi;

import java.util.Objects;

public class StringDocument implements Document {

  private final String title;
  private final String body;

  private final DocumentId id;

  public StringDocument(DocumentId id, String title, String body) {
    this.id = id;
    this.title = title;
    this.body = body;
  }

  @Override
  public DocumentId getId() {
    return this.id;
  }

  @Override
  public String getTitle() {
    return this.title;
  }

  @Override
  public String getBody() {
    return this.body;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StringDocument that = (StringDocument) o;
    return Objects.equals(title, that.title) && Objects.equals(body, that.body);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, body);
  }

  @Override
  public String toString() {
    return "StringDocument{" + "title='" + title + '\'' + ", id=" + id + '}';
  }
}
