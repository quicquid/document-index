package io.github.clementi;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DocumentIndex implements Index {

  private final Indexer indexer;
  private final Map<Term, Set<TermReference>> dictionary;

  public DocumentIndex(DocumentIndexer indexer) {
    this.indexer = indexer;
    this.dictionary = new HashMap<>();
  }

  @Override
  public void add(Document document) {
    var terms = getTerms(document);

    terms.forEach(term -> {
      var references = indexer.index(document, term);
      var existingReferences = this.dictionary.getOrDefault(term, new HashSet<>());
      existingReferences.addAll(references);
      this.dictionary.put(term, existingReferences);
    });
  }

  private Set<Term> getTerms(Document document) {
    return Arrays.stream(document.getBody().split(" ")).map(Term::new)
        .collect(Collectors.toUnmodifiableSet());
  }

  @Override
  public Map<DocumentId, Set<Integer>> search(Term term) {
    var results = this.dictionary.get(term);

    if (results == null) {
      return Map.of();
    }

    return Collections.unmodifiableMap(results.stream()
        .map(result -> new Pair<>(result.getId(), result.getPosition())).collect(Collectors
            .groupingBy(Pair::getFirst, Collectors.mapping(Pair::getSecond, Collectors.toSet()))));

  }

  @Override
  public Set<Term> getTerms() {
    return Collections.unmodifiableSet(this.dictionary.keySet());
  }

  @Override
  public Set<TermReference> getDocumentReferences() {
    return this.dictionary.values().stream().flatMap(Collection::stream)
        .collect(Collectors.toUnmodifiableSet());
  }
}
