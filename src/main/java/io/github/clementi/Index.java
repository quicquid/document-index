package io.github.clementi;

import java.util.Map;
import java.util.Set;

public interface Index {

  void add(Document document);

  Map<DocumentId, Set<Integer>> search(Term term);

  Set<Term> getTerms();

  Set<TermReference> getDocumentReferences();
}
