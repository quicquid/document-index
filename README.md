# document-index

A toy project for investigating the inverted index data structure.

This project consists of an indexer, an index, various document types, a document store, terms, term
references, and a simple REPL to query the index.